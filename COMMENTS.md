Hi Michael,

I ran into a lot of difficuly getting the application to run.
All unit tests are failing (see test_output.txt), I think there is a libraray that is out of date, or there is something weird with my setup.
I couldn't get to the bottom of it. Normally about here I would be asking for help. 

With the tests failing to work, I turned to postman which also didn't work. I'm really confused and tearing my hair out at this point. 
After 40 minutes I gave up on trying to make the code worked and focussed on the task as best I could, sticking to the 1 hour limit.

I believe I have found and fixed the bug. I have also had a solid go at the feature request. Since I cannot run my code it is no doubt syntactically incorrect. For the sake of time I also skipped writting a unit test. 

I hope this problem doesn't overshadow my answer to the problem. I would be happy to talk you through my solution. 

================================================

2nd pass.
I'ts almost midnight. The bug has been fixed, the float/decimal conversion being the root cause.
I am happy with the structure of the new post functionality, but I cannot make the post itself work. 
My code outlines the concept even if it doesn't work. If I had more time I would spend it learning more about the Flask library to see if it contains the functionality I need to actually make the post work.


